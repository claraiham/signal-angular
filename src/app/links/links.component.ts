import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-links',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './links.component.html',
  styleUrl: './links.component.css'
})

export class LinksComponent {

  links = [
      {
        name: "Get signal"
      },
      {
        name: "Help"
      },
      {
        name: "Blog"
      },
      {
        name: "Developpers"
      },
      {
        name: "Careers"
      },
      {
        name: "Donate"
      }
    ] 
}
