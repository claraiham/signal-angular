import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { CardComponent } from './card/card.component';
import { BanniereComponent } from './banniere/banniere.component';
import { SecurityComponent } from './security/security.component';
import { FooterComponent } from './footer/footer.component';
import { InformationComponent } from './information/information.component';
import { InformationListComponent } from './information-list/information-list.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavigationComponent, CardComponent, BanniereComponent, SecurityComponent, FooterComponent, InformationComponent, InformationListComponent],
  templateUrl: './app.component.html',

  
  styleUrl: './app.component.css',
})
export class AppComponent{

}