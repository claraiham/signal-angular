import { Component } from '@angular/core';
import { LinksComponent } from '../links/links.component';
import { BurgermenuComponent } from '../burgermenu/burgermenu.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';

@Component({
  selector: 'app-navigation',
  standalone: true,
  imports: [LinksComponent, BurgermenuComponent, FlexLayoutModule, FlexLayoutServerModule],
  templateUrl: './navigation.component.html',
  styleUrl: './navigation.component.css'
})
export class NavigationComponent {
  active = false;
}
