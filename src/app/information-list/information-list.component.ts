import { Component } from '@angular/core';
import { InformationComponent } from '../information/information.component';

@Component({
  selector: 'app-information-list',
  standalone: true,
  imports: [InformationComponent],
  templateUrl: './information-list.component.html',
  styleUrl: './information-list.component.css'
})
export class InformationListComponent {
  information = [
    {
      id:1,
      title: "Aucune publicité, aucun traqueur, vraiment.",
    img: "./assets/No-Ads.png",
    description: "Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni aucun système de suivi inquiétant. Vous pouvez vous consacrer à partager les moments importants avec les personnes qui comptent pour vous."
  },
  {
    id: 2,
    title: "Gratuit pour tous",
    img: "./assets/Nonprofit503.png",
    description: "Signal est un organisme à but non lucratif indépendant. Nous ne sommes reliés à aucune entreprise technologique importante et nous ne pourrons jamais être achetés par l’une d’elles. Le développement de notre plateforme est financé par des subventions et des dons de personnes comme vous. "
  }
  ]
}
