export type LastComponent =
 {
    id: number
    title: string
    img: string
    description: string
}
