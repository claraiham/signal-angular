import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
// CommonModule me permet de pouvoir passer les propriétés de mon composant dans mon html
@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card.component.html',
  styleUrl: './card.component.css'
})

//Je fais un tableau d'objet sur lequel je vais boucler à partir de mon html avec *ngFor="let blabla of Array", grâce à CommonModule
export class CardComponent {
  events = [
    {
      img: "./assets/Stickers.png",
      title: "Respectez votre vie privée",
      desc:"Avec nos stickers chiffrés, exprimez-vous encore plus librement. Vous êtes même libre de créer et de partager vos propres packs de stickers."
    },
    {
      img: "./assets/Media.png",
      title: "Dites ce qui vous plaît",
      desc:"Partagez gratuitement des textes, des messages vocaux, des photos, des vidéos et des GIF. Signal utilise la connexion de données de votre téléphone afin de vous éviter les frais relatifs à l’envoi de texto et de message multimédia."
    },
    {
      img: "./assets/Calls.png",
      title: "Exprimez-vous librement",
      desc:"Passez des appels vocaux ou vidéo clairs avec des personnes qui vivent à l’autre bout de la ville ou du monde, sans frais supplémentaires"
    },
    {
      img: "./assets/Groups.png",
      title: "Rassemblez-vous avec les groupes",
      desc:"Avec les conversations de groupe, restez facilement en contact avec votre famille, vos amis et vos collègues."
    }
  ]
}
