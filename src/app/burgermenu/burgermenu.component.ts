import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-burgermenu',
  standalone: true,
  imports: [],
  templateUrl: './burgermenu.component.html',
  styleUrl: 'burger-menu.component.scss'
})
export class BurgermenuComponent implements OnInit{
  @Input() init: boolean | undefined;
  @Output() opened = new EventEmitter<any>();

  active = false;

  ngOnInit() {
    this.active = this.init || false;
  }

  onBurgerClicked() {
    this.active = !this.active;
    this.opened.emit();
  }
}
