import { Component, Input} from '@angular/core';
import { LastComponent } from '../last-component';


@Component({
  selector: 'app-information',
  standalone: true,
  imports: [],
  templateUrl: './information.component.html',
  styleUrl: './information.component.css'
})
export class InformationComponent {
 @Input()
 info?: LastComponent
}
